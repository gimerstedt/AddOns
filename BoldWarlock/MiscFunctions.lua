function GarbageWarlockBuffs()
	local clearTarget = false
	if UnitExists("target") ~= 1 then
		TargetUnit("player")
		clearTarget = true
	end
	if not BC.HasBuffByName("target", "Unending Breath") then
		CastSpellByName("Unending Breath")
	end
	if not BC.HasBuffByName("target", "Detect Lesser Invisibility") then
		CastSpellByName("Detect Lesser Invisibility")
	end
	if not BC.HasBuffByName("target", "Detect Invisibility") then
		CastSpellByName("Detect Invisibility")
	end
	if not BC.HasBuffByName("target", "Detect Greater Invisibility") then
		CastSpellByName("Detect Greater Invisibility")
	end
	if clearTarget then
		ClearTarget()
	end
end
