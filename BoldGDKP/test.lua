-- dependencies
lu = require('luaunit')

-- mock
SlashCmdList = {}

-- class under test
require('BoldGDKP')

-- tests
function testExplode()
	local split, str = {}, "a b c"
	table.insert(split, "a")
	table.insert(split, "b")
	table.insert(split, "c")
	lu.assertEquals(split, explode(" ", str))
end

function testFirstToUpper()
	lu.assertEquals("Asdf", firstToUpper("asdf"))
end

-- gogo
os.exit( lu.LuaUnit.run() )
