-- savedvars
BoldGDKPItems = {}
BoldGDKPOptions = {}

-- prepend for chat print
local prep = "[BoldGDKP] "

local function rm(arg)
	if(table.getn(BoldGDKPItems) >= tonumber(arg)) then
		table.remove(BoldGDKPItems, arg)
		BC.m("Entry #"..arg.." has been removed.", prep)
	else
		BC.m("No such entry.", prep)
	end
end

local function ls()
	if(table.getn(BoldGDKPItems) > 0) then
		BC.m("Current entries:", prep)
		local total = 0
		for k,v in pairs(BoldGDKPItems) do
			if(v.price and v.recipient and v.item) then
				total = total + v.price
				BC.mw(k..": "..v.price.."g, "..v.recipient..", "..v.item, prep)
			else
				rm(k)
			end
		end
		BC.m("Total: "..total.."g.", prep)
	else
		BC.m("No entries found.", prep)
	end
end

local function add(price, recipient, allArgs)
	price = tonumber(price)
	if price and string.find(allArgs, "%[") then
		local item = string.sub(string.gsub(string.gsub(allArgs, price, ""),recipient, ""), 3)
		local _, b = string.find(item, "%[")
		item = string.gsub(string.gsub(string.sub(item, b), "%[", ""), "%]", "")

		recipient = firstToUpper(recipient)

		local entry = {}
		entry.price = price
		entry.recipient = recipient
		entry.item = item

		table.insert(BoldGDKPItems, entry)
		local s = "New entry: "..price.."g, "..recipient..", "..item
		BC.m(s, prep)
	else
		BC.mr("Bad dog!", prep)
	end
end

local function help()
	BC.mp("Stuff stuff stuff...", prep)
	BC.my("/bgd ls", prep)
	BC.mr("List current entries.", prep)
	BC.my("/bgd 100 Boldilocks [ItemLink]", prep)
	BC.mr("Add an entry.", prep)
	BC.my("/bgd rm #", prep)
	BC.mr("Remove entry at #.", prep)
	BC.my("/bgd clear", prep)
	BC.mr("Clear all entries.", prep)
	BC.my("/bgd report", prep)
	BC.mr("Reports in /raid the current total and split.", prep)
	BC.my("/bgd ty", prep)
	BC.mr("Says thanks for the raid and special thanks to buyers.", prep)
	BC.my("/bgd split #", prep)
	BC.mr("Specify the split in percentages.")
end

local function clear()
	BoldGDKPItems = {}
	BC.m("All entries cleared.")
end

local function report()
	local numberOfItems = table.getn(BoldGDKPItems)
	if numberOfItems > 0 then
		local total = 0
		local totalAfterSplit = 0
		local raiders = GetNumRaidMembers()
		for k,v in pairs(BoldGDKPItems) do
			total = total + v.price
		end
		if BoldGDKPOptions.split then
			totalAfterSplit = total * BoldGDKPOptions.split / 100
		end
		local split = math.floor(totalAfterSplit / raiders)
		BC.r("A total of "..total.."g has been collected thus far on "..numberOfItems.." items resulting in "..split.."g each with a "..BoldGDKPOptions.split.."% split ("..raiders.." raiders).")
	else
		BC.m("Nothing to report.")
	end
end

local function ty()
	local buyers = "";
	for k,v in pairs(BoldGDKPItems) do
		buyers = buyers..v.recipient..", "
	end
	buyers = string.sub(buyers, 1, -3)
	BC.r("Thanks to everyone for coming and a special thanks to our buyers: ")
	BC.r(buyers)
	BC.r("I hope to see you all next week!")
end

local function setSplit(split)
	split = tonumber(split)
	if split then
		BoldGDKPOptions.split = split
	end
end

local function init()
	if not BoldGDKPOptions.split then
		BoldGDKPOptions.split = 95
	end
end

-- info command
SLASH_BOLDGDKP1, SLASH_BOLDGDKP2 = '/bgdkp', '/bgd'
function SlashCmdList.BOLDGDKP(arg)
	init()
	arrg = explode(" ", string.lower(arg))
	if arrg[1] == "" or arrg[1] == "help" then
		help()
	elseif arrg[1] == "ls" then
		ls()
	elseif arrg[1] == "rm" then
		rm(arrg[2])
	elseif arrg[1] == "clear" then
		clear()
	elseif arrg[1] == "report" then
		report()
	elseif arrg[1] == "ty" then
		ty()
	elseif arrg[1] == "split" then
		setSplit(arrg[2])
	else
		add(arrg[1], arrg[2], arg)
	end
end

function explode(div,str)
	if (div=='') then return false end
	local pos,arr = 0,{}
	for st, sp in function()
		return string.find(str,div,pos,true)
	end
	do
		table.insert(arr,string.sub(str,pos,st-1))
		pos = sp + 1;
	end
	table.insert(arr,string.sub(str,pos))
	return arr
end

function firstToUpper(str)
	return string.upper(string.sub(str, 1, 1))..string.sub(str, 2)
end
